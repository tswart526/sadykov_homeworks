import java.util.Scanner;

public class Program {

    public static void main(String[] args) {
       
        Scanner scanner = new Scanner(System.in);
        int minDig = 10;
        int a = scanner.nextInt();

        while (a != -1) {
            while (a != 0) {
                if (a % 10 < minDig) {
                    minDig = a % 10;
                }
                a = a / 10;
            }
            a = scanner.nextInt();
        }
        System.out.println("Answer is: " + minDig);
    }
}
